import React, { Component, PropTypes } from 'react'
import SiteHeader from './SiteHeader'
import SiteDescription from './SiteDescription'
import SiteLinks from './SiteLinks'

class SiteItem extends Component {
  render() {
    return (
      <div>
        <SiteHeader title={this.props.site.title} />
        <SiteDescription description={this.props.site.description} />
        <SiteLinks homepage={this.props.site.homepage} docs={this.props.site.docs} />
      </div>
    )
  }
}

SiteItem.propTypes = {
  site: PropTypes.object.isRequired
}

export default SiteItem
