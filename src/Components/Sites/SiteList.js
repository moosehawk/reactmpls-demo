import React, { Component, PropTypes } from 'react'
import SiteItem from './SiteItem'

class SiteList extends Component {
  render() {
    const lowerCaseFilterText = this.props.filterText.toLowerCase()
    const visibleSites = this.props.sites.filter(site =>
      site.title.toLowerCase().indexOf(lowerCaseFilterText) > -1
      || site.description.toLowerCase().indexOf(lowerCaseFilterText) > -1
    )

    return (
      <div>
        { visibleSites.map(site => (
          <SiteItem key={site.id} site={site} />
        ))}
      </div>
    )
  }
}

SiteList.propTypes = {
  filterText: PropTypes.string.isRequired,
  sites: PropTypes.array.isRequired
}

export default SiteList
