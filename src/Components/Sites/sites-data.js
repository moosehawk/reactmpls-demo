export default [
  {
    id: 1,
    homepage: 'https://facebook.github.io/react/',
    docs: 'https://facebook.github.io/react/docs/hello-world.html',
    title: 'React Homepage',
    description: 'A JavaScript library for building user interfaces.'
  }, {
    id: 2,
    homepage: 'https://github.com/facebookincubator/create-react-app',
    docs: 'https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md',
    title: 'Create-react-app',
    description: 'Create react apps with no build configuration.'
  }, {
    id: 3,
    homepage: 'https://react-bootstrap.github.io/',
    docs: 'https://react-bootstrap.github.io/components.html',
    title: 'React Bootstrap',
    description: 'The most popular front-end framework, rebuilt for React.'
  }
]