import React, { Component, PropTypes } from 'react'

class SiteFilterInput extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange() {
    this.props.onFilterInput(this.refs.filterTextInput.value)
  }

  render() {
    return (
      <form>
        <input
          ref="filterTextInput"
          type="text"
          placeholder="Filter..."
          onChange={this.handleChange}
          value={this.props.filterText} />
      </form>
    )
  }
}

SiteFilterInput.propTypes = {
  filterText: PropTypes.string.isRequired,
  onFilterInput: PropTypes.func.isRequired
}

export default SiteFilterInput
