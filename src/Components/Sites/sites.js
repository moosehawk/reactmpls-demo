import React, { Component } from 'react'
import SiteFilterInput from './SiteFilterInput'
import SiteList from './SiteList'
import sites from './sites-data'

class Sites extends Component {
  constructor(props) {
    super(props)

    this.state = {
      filterText: '',
      sites
    }

    this.handleFilterInput = this.handleFilterInput.bind(this)
  }

  handleFilterInput(filterText) {
    this.setState({ filterText })
  }

  render() {
    return (
      <div>
        <SiteFilterInput onFilterInput={this.handleFilterInput} filterText={this.state.filterText} />
        <SiteList sites={this.state.sites} filterText={this.state.filterText} />
      </div>
    )
  }
}

export default Sites
