import React, { Component, PropTypes } from 'react'

class SiteDescription extends Component {
  render() {
    return (
      <div>
        {this.props.description}
      </div>
    )
  }
}

SiteDescription.propTypes = {
  description: PropTypes.string.isRequired
}

export default SiteDescription
