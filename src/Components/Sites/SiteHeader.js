import React, { Component, PropTypes } from 'react'

class SiteHeader extends Component {
  render() {
    return (
      <span>
        <strong>{this.props.title}</strong>
      </span>
    )
  }
}

SiteHeader.propTypes = {
  title: PropTypes.string.isRequired
}

export default SiteHeader
