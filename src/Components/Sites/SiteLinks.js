import React, { Component, PropTypes } from 'react'

class SiteLinks extends Component {
  render() {
    return (
      <div>
        <a href={this.props.homepage} target="_blank">Home</a> | <a href={this.props.docs} target="_blank">Docs</a>
      </div>
    )
  }
}

SiteLinks.propTypes = {
  homepage: PropTypes.string.isRequired,
  docs: PropTypes.string.isRequired
}

export default SiteLinks
